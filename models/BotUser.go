package models

type BotUser struct {
	TelegramId int `json:"telegramId"`
	Phone int `json:"phone"`
	Name string `json:"name"`
	MacAddress string `json:"macAddress"`
	IsAdmin bool `json:"isAdmin"`

}

