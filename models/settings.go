package models

import (
	"io/ioutil"
	"encoding/json"
	"log"
)

type Settings struct {
	BroadcastAddress string `json:"broadcast_address"`
	TelegramToken string `json:"telegram_token"`
}

var settings Settings

func GetSettings() Settings  {
	if len(settings.TelegramToken) == 0{
		err := readSettingsFromDisk(); if err != nil {
			panic("Settings not found!\nCheck file settings.json is exist and valid")
		}
	}
	log.Println(settings)
	return settings
}

func readSettingsFromDisk() error{
	raw, err := ioutil.ReadFile("settings.json")
	if err != nil {
		return err
	}
	err = json.Unmarshal(raw, &settings); if err != nil {
		return err
	}
	return nil
}