package models

import (
	"gopkg.in/zabawaba99/firego.v1"
	"log"
	"io/ioutil"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"errors"
	"strconv"
)

var firebase *firego.Firebase

func initFirebase() error  {
	base, err := createConnectionWithToken()
	if(err != nil){
		panic("Shiiiiiiiiiiit!")
		return errors.New("Connection error")
	}
	firebase = base
	user, err := getUserByTelegramId(12); if err != nil{
		log.Print(user)
	}
	return nil
}

func createConnectionWithToken() (*firego.Firebase, error) {

	d, err := ioutil.ReadFile("account.json")
	if err != nil {
		return nil, errors.New("Problem with account.json")
	}

	conf, err := google.JWTConfigFromJSON(d, "https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/firebase.database")
	if err != nil {
		return nil, errors.New("Problem with JWT")
	}

	return firego.New("https://forasoftservice.firebaseio.com/users", conf.Client(oauth2.NoContext)), nil
}

func getUserByTelegramId(telegramId int)(*BotUser, error){
	var user BotUser
	var v map[string]BotUser
	if err := firebase.EqualToValue(telegramId).OrderBy("telegramId").Value(&v); err != nil {
		log.Fatal(err)
		return nil, errors.New("DB error")
	}
	if(len(v) == 0){
		return nil, errors.New("No found")
	}
	for _, element := range v {
		user = element
		break
	}
	return &user, nil

}

func getUserByPhone(phone string)(*BotUser, error){
	var user BotUser
	if err := firebase.Child(phone).Value(&user); err != nil {
		log.Fatal(err)
		return nil, errors.New("DB error")
	}
	return &user, nil
}

func updateUser(user *BotUser) error  {
	return firebase.Child(strconv.Itoa(user.Phone)).Update(user)
}



