package models

import (
	"os"
	"errors"
)

func Init()  {
	err := initFirebase(); if err != nil{
		panic("No firebase connection")
		os.Exit(-1)
	}
}


func Register(phone string, telegramId int) error  {
	user, err := getUserByPhone(phone); if err != nil{
		return errors.New("User with this phone not exist")
	}
	user.TelegramId = telegramId
	return updateUser(user)
}

func GetUser(telegramId int) (*BotUser, error)  {
	return getUserByTelegramId(telegramId)
}

func SetMac(user *BotUser, macAddress string) error {
	user.MacAddress = macAddress
	return updateUser(user)
}
