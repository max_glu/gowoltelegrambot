package routers

import (
	"gopkg.in/telegram-bot-api.v4"
	"gitlab.com/max_glu/ForaSoftGoBot/models"
	"gitlab.com/max_glu/ForaSoftGoBot/utils"
)

func prepareWakeupReply(user *models.BotUser, reply *tgbotapi.MessageConfig){
	if isMacValid(user.MacAddress){
		utils.WakeOnLan(user.MacAddress)
		reply.Text = "I sent wakeup message to your computer. Wait for few minutes, then try to connect to it."
	} else {
		reply.Text = "You need to send me your mac address before using this command.\n" +
			"The right way to send correct mac is (example): " +
			"/set_mac D8:D8:8A:11:D8:D8\n" +
			"or\n" +
			"/set_mac D8-D8-8A-11-D8-D8"
	}
}