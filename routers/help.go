package routers

import "gopkg.in/telegram-bot-api.v4"

func prepareHelpReply(reply *tgbotapi.MessageConfig){
	str := "Hi! I am a service bot.\n" +
		"If you are working from home, I can help you with waking up your computer. " +
		"Check out how I can help you:\n" +
		"/"+ start + " - Attempt to try all over again\n" +
		"/"+ help + " - Show available commands\n" +
		"/"+ wakeup + " - Send wake up command to your computer\n" +
		"/"+ set_mac + " - Set your current mac (E.g. /set_mac D8:D8:8A:11:D8:D8) \n" +
		"/"+ show_mac + " - Display your current mac\n" +
		"\n" +
		"PS: If you got sick, get well!"
	reply.Text = str
}
