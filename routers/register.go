package routers

import (
	"gopkg.in/telegram-bot-api.v4"
	"gitlab.com/max_glu/ForaSoftGoBot/models"
)

func prepareNeedRegistrationReply(reply *tgbotapi.MessageConfig) {
	reply.Text = "Hello! Before we start, I need to get your phone number.\n" +
		"Please click on \"Share phone number\" button."
	keyBoardButton := tgbotapi.NewKeyboardButtonContact("Share phone number")
	replyKeyboards := tgbotapi.NewReplyKeyboard([]tgbotapi.KeyboardButton{keyBoardButton})
	reply.ReplyMarkup = replyKeyboards
	return
}

func prepareContactReceivedReply(update *tgbotapi.Update, reply *tgbotapi.MessageConfig){
	if update.Message.From.ID != update.Message.Contact.UserID {
		reply.Text = "You can send me only your own phone number. Other contacts are not allowed."
		return
	}

	_, err := models.GetUser(update.Message.From.ID); if err != nil {
		userByPhoneErr := models.Register(update.Message.Contact.PhoneNumber, update.Message.From.ID)
		if userByPhoneErr != nil {
			prepareNotEmployeeReply(reply)
		} else {
			reply.Text = "Thank you! Now we are ready to work :)"
		}
	} else {
		prepareAlreadyRegisteredReply(reply)
	}

	keyBoardButton := tgbotapi.NewRemoveKeyboard(false)
	reply.ReplyMarkup = keyBoardButton
}

func prepareAlreadyRegisteredReply(reply *tgbotapi.MessageConfig)  {
	reply.Text = "We already met, I don't want to start all over again :)\n" +
		"Maybe you need /help ?"
}

func prepareNotEmployeeReply(reply *tgbotapi.MessageConfig)  {
	reply.Text = "Looks like your phone number is not registered in the system yet.\n" +
		"If you think that this is a mistake please find the guy with ginger beard :|"
}
