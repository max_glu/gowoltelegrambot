package routers

import (

	"gopkg.in/telegram-bot-api.v4"
	"gitlab.com/max_glu/ForaSoftGoBot/models"
)

func prepareShowMacReply(user *models.BotUser, reply *tgbotapi.MessageConfig)  {
	mac := user.MacAddress
	reply.Text = "Your current mac address is " + mac
}