package routers

import (
	"gopkg.in/telegram-bot-api.v4"
	"github.com/asaskevich/govalidator"
	"strings"
	"regexp"
	"gitlab.com/max_glu/ForaSoftGoBot/models"
)

func prepareSetMacReply(user *models.BotUser, mac string, reply *tgbotapi.MessageConfig)  {
	mac = strings.ToUpper(mac)
	if(isMacValid(mac)){
		if strings.Contains(mac, "-"){
			mac = replaceMinusToColon(mac)
		}
		err := models.SetMac(user, mac); if err != nil{
			reply.Text = "Ooops, seems like something is broken. Please try again later.\n" +
				"Or find Max."
		} else {
			reply.Text = "Ok, your new mac address is " + mac
		}
	} else {
		reply.Text = "You entered invalid mac address.\n" +
			"The right way to send correct mac is (example): " +
			"/set_mac D8:D8:8A:11:D8:D8\n" +
			"or\n" +
			"/set_mac D8-D8-8A-11-D8-D8"
	}
}

func replaceMinusToColon(str string) string  {
	re := regexp.MustCompile("-")
	return re.ReplaceAllString(str, ":")
}

func isMacValid(mac string) bool  {
	return govalidator.IsMAC(mac)
}