package routers

import (
	"log"
	"gopkg.in/telegram-bot-api.v4"
	"gitlab.com/max_glu/ForaSoftGoBot/models"
)

const (
	help = "help"
	start  = "start"
	wakeup = "wakeup"
	set_mac = "set_mac"
	show_mac = "show_mac"
)
var bot *tgbotapi.BotAPI
func PrepareWithBot(initializedBot *tgbotapi.BotAPI) {
	bot = initializedBot
}

func RouteUpdate(update *tgbotapi.Update) {
	log.Printf("[%s] %s", update.Message.From.FirstName + " " + update.Message.From.LastName, update.Message.Text)
	userId := update.Message.From.ID
	reply := tgbotapi.NewMessage(update.Message.Chat.ID, "")

	isNeedIgnoreCommand := false

	//Check if contact received for complete registration
	if update.Message.Contact != nil {
		prepareContactReceivedReply(update, &reply)
		isNeedIgnoreCommand = true
	}

	//Check not registered request and attempt register
	user, err := models.GetUser(userId); if err != nil {
		prepareNeedRegistrationReply(&reply)
		isNeedIgnoreCommand = true
	}

	if update.Message.IsCommand() && !isNeedIgnoreCommand{
		command := update.Message.Command()
		arguments := update.Message.CommandArguments()
		switch command {
		case help:
			prepareHelpReply(&reply)
			break
		case start:
			//prepareNeedRegistrationReply(&reply)
			prepareAlreadyRegisteredReply(&reply)
			break
		case set_mac:
			prepareSetMacReply(user, arguments, &reply)
			break
		case show_mac:
			prepareShowMacReply(user, &reply)
			break
		case wakeup:
			prepareWakeupReply(user, &reply)
			break
		default:
			reply.Text = "Sorry, I don't underastand waht you mean :("
			break
		}
	}

	bot.Send(reply)
}


