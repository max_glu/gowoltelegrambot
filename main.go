package main

import (

	"gopkg.in/telegram-bot-api.v4"
	"log"
	"gitlab.com/max_glu/ForaSoftGoBot/routers"
	"gitlab.com/max_glu/ForaSoftGoBot/models"
)
func main()  {
	models.Init()
	bot, err := tgbotapi.NewBotAPI(models.GetSettings().TelegramToken)
	if(err != nil){
		log.Panic(err)
	}

	//bot.Debug = true
	log.Printf("Authorized on account %s", bot.Self.UserName)
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	routers.PrepareWithBot(bot)

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {

		if update.Message == nil {
			continue
		}

		go routers.RouteUpdate(&update)
	}
}
