package utils

import (

	"log"
	"gitlab.com/max_glu/ForaSoftGoBot/models"
	"gitlab.com/max_glu/ForaSoftGoBot/wol"
)

func WakeOnLan(mac string) error  {
	log.Printf("Sending wol packet to broadcast address %s...", models.GetSettings().BroadcastAddress)
	err := wol.MagicWake(mac, models.GetSettings().BroadcastAddress)

	return err
}
